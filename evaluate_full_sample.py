import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os
import cv2
import time

from patch_net import get_patch_net2
from unet_512_768 import get_unet_512
from dnn_utils import patch_generator, evaluate_patch_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index
from dnn_utils import eval_jaccard, eval_acc, evaluation_generator


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################

BATCH_SIZE = 1024
LR=0.0001
DECAY=0.002

ori_path = 'images/sfa/ORI'
gt_path = 'images/sfa/GT'

img_files = os.listdir(ori_path)

test_subjects = list(range(951,1119))

test_img =  [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in  test_subjects]
test_gt =  [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in  test_subjects]

patch_datagen = patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)

patch_model = get_patch_net2()
patch_model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

unet_model = get_unet_512()
unet_model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

################################################
print("Predicting...")
################################################

patch_model_name = "patch2_full_LR0.0001_DC0.002_66"

patch_model.load_weights(patch_model_name + ".hdf5")

unet_model_name = "unet_LR0.0001_DC0.002_43"

unet_model.load_weights(unet_model_name + ".hdf5")


patch_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess)

unet_datagen = evaluation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)


TEST_STEPS = len(test_img)

N_THUMBS = 12

acc_patch, jac_patch = 0,0
acc_unet, jac_unet = 0,0

tn_h, tn_w = 576//4, 768//4
grid_size = (6,8)

im_result = np.zeros((tn_h*grid_size[0],tn_w*grid_size[1],3),np.uint8)

for i in range(TEST_STEPS):

    print("\nPredicting image %s" % test_img[i])

    img = cv2.imread(test_img[i])
    img = cv2.resize(img,(tn_w,tn_h))

    # Patch Prediction

    start_time = time.time()
    patch, mask = next(patch_datagen)

    print(" .")
    cv2.imshow("Result", im_result)
    c = cv2.waitKey(3)

    patch_prediction = patch_model.predict(patch)
    a = eval_acc(mask, patch_prediction)
    j = eval_jaccard(mask, patch_prediction)
    print("Patch Prediction Acc: %.4f  Jaccard Index: %.4f Time: %.2f seconds" % (a, j, time.time() - start_time))
    acc_patch += a
    jac_patch += j

    cv2.imshow("Result", im_result)
    c = cv2.waitKey(3)

    # UNet Prediction

    start_time = time.time()
    _img, _mask = next(unet_datagen)
    unet_prediction = unet_model.predict(_img)

    unet_prediction=unet_prediction[0]

    # fixing size

    if unet_prediction.shape[0] != mask.shape[0]:

        pred_corr = np.zeros(mask.shape)

        (h1, w1) = unet_prediction.shape[:2]
        (h2, w2) = mask.shape[:2]

        pred_corr[:, (w2 - w1) // 2: (w2 - w1) // 2 + w1] = unet_prediction[(h1 - h2) // 2: (h1 - h2) // 2 + h2, :]
        unet_prediction = pred_corr


    a = eval_acc(mask, unet_prediction)
    j = eval_jaccard(mask, unet_prediction)
    print("U-Net Prediction Acc: %.4f  Jaccard Index: %.4f Time: %.2f seconds" % (eval_acc(mask, unet_prediction), eval_jaccard(mask, unet_prediction),time.time() - start_time))
    acc_unet += a
    jac_unet += j

    # Thumbnails

    if i<N_THUMBS:

        lin = (i*4)//grid_size[1]
        col = (i*4)%grid_size[1]

        y_start = lin * tn_h
        x_start = col * tn_w

        print("i %d lin %d col %d y_s %d x_s %d" % (i, lin, col, y_start, x_start) )

        im_result[y_start:y_start+tn_h, x_start:x_start+tn_w] = img

        patch_pred = cv2.resize((patch_prediction .reshape(mask.shape) * 255).astype(np.uint8),(tn_w,tn_h))
        unet_pred = cv2.resize((unet_prediction * 255).astype(np.uint8),(tn_w,tn_h))

        mask = cv2.resize((mask*255).astype(np.uint8),(tn_w,tn_h))

        for channel in range(3):
            im_result[y_start:y_start+tn_h, x_start+tn_w:x_start+2*tn_w, channel] = mask
            im_result[y_start:y_start+tn_h, x_start+2*tn_w:x_start+3*tn_w, channel] = patch_pred
            im_result[y_start:y_start+tn_h, x_start+3*tn_w:x_start+4*tn_w, channel] = unet_pred

        cv2.imshow("Result", im_result)
        c = cv2.waitKey(3)
    elif i==N_THUMBS:
        cv2.imshow("Result", im_result)
        cv2.destroyAllWindows()
        cv2.imwrite("full_sample.png", im_result)

acc_patch, jac_patch = acc_patch/TEST_STEPS, jac_patch/TEST_STEPS
acc_unet, jac_unet = acc_unet/TEST_STEPS, jac_unet/TEST_STEPS

print("\nFinal Results. Acc: %.4f  Jaccard Index: %.4f" % (acc_patch, jac_patch))
print("Patch - Acc: %.4f  Jaccard Index: %.4f" % (acc_patch, jac_patch))
print("Unet  - Acc: %.4f  Jaccard Index: %.4f" % (acc_unet, jac_unet))

cv2.imshow("Result", im_result)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()

