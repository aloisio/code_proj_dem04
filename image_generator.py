import threading
import cv2
import numpy as np
from sklearn.utils import shuffle

THRESHOLD = 5


#################### Now make the data generator threadsafe ####################


def skin_preprocess(img):
    im = np.empty(img.shape)
    X = img[:,:,0]
    Y = img[:,:,1]
    Z = img[:,:,2]
    im[:,:,0] = (X.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    im[:,:,1] = (Y.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    im[:,:,2] = (Z.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    return im

def mask_preprocess(img):
    im = np.zeros((img.shape[0],img.shape[1]))
    im[(img[:,:,0] + img[:,:,1] + img[:,:,2]) >THRESHOLD] = 1
    return im


class threadsafe_generator(object):
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, gen):
        self.gen = gen
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.gen.__next__()


def threadsafe(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_generator(f(*a, **kw))
    return g

@threadsafe
def segmentation_generator(img_files, mask_files=None,
                           img_preproc_func=skin_preprocess, mask_preproc_func=None, batch_size=4,
                           shuffle=False):  # write the definition of your data generator

    x_batch = []
    y_batch = []
    batch_count = 0
    while True:
        if shuffle:
            img_files_s, mask_files_s = shuffle(img_files, mask_files)
        else:
            img_files_s, mask_files_s = img_files, mask_files

        for count, (img_file, mask_file) in enumerate(zip(img_files_s, mask_files_s)):
            img = img_preproc_func(cv2.imread(img_file))
            mask = mask_preproc_func(cv2.imread(mask_file))
            x_batch.append(img)
            y_batch.append(mask)
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch
                x_batch = []
                y_batch = []
                batch_count = 0


########## Data generator is now threadsafe and should work with multiple workers ##########

import keras.backend as K

def jaccard_index(y_true, y_pred):
    smooth = 1
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    union = K.sum(y_true_f) + K.sum(y_true_f) - intersection
    score = (intersection + smooth) / (union + smooth)
    return score

from keras.callbacks import TensorBoard

def get_callbacks(model_name, log_dir="/tmp/tensor_board", patience=30):
    es = EarlyStopping('val_loss', patience=patience, mode="min")
    msave = ModelCheckpoint(model_name+'.hdf5', save_best_only=True, save_weights_only=True)
    log = TensorBoard(log_dir=log_dir+"/"+model_name)
    red = ReduceLROnPlateau(patience=patience//5, verbose=1, cooldown=patience//6)
    return [es, msave, log, red]

def get_run(dir):
    if os.path.exists(dir+"/runcount.csv"):
        rc = pd.read_csv(dir+"/runcount.csv")
    else:
        rc=pd.DataFrame(data={'run':['0']}, dtype='int')
    run = rc.run[0]
    run += 1
    rc.run = [run]
    rc.to_csv(dir+"/runcount.csv", index=False)
    return run
