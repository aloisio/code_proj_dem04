import threading
import cv2
import numpy as np
from sklearn.utils import shuffle
import pandas as pd
import os
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

THRESHOLD = 5


#################### Now make the data generator threadsafe ####################


def randomHueSaturationValue(image, hue_shift_limit=(-100, 100),
                             sat_shift_limit=(-100, 100),
                             val_shift_limit=(-100, 100), u=0.8):
    if np.random.random() < u:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        h, s, v = cv2.split(image)
        hue_shift = np.random.uniform(hue_shift_limit[0], hue_shift_limit[1])
        h = cv2.add(h, hue_shift)
        sat_shift = np.random.uniform(sat_shift_limit[0], sat_shift_limit[1])
        s = cv2.add(s, sat_shift)
        val_shift = np.random.uniform(val_shift_limit[0], val_shift_limit[1])
        v = cv2.add(v, val_shift)
        image = cv2.merge((h, s, v))
        image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

    return image


def randomShift(image, mask,
                           shift_limit=(-0.09, 0.09),
                           u=0.8):
    if np.random.random() < u:
        height, width, channel = image.shape

        dx = round(np.random.uniform(shift_limit[0], shift_limit[1]) * width)
        dy = round(np.random.uniform(shift_limit[0], shift_limit[1]) * height)

        M = np.float32([[1, 0, dx], [0, 1, dy]])
        image = cv2.warpAffine(image, M, (width, height))
        mask = cv2.warpAffine(mask, M, (width, height))

    return image, mask


def randomHorizontalFlip(image, mask, u=0.5):
    if np.random.random() < u:
        image = cv2.flip(image, 1)
        mask = cv2.flip(mask, 1)

    return image, mask

def randomVerticalFlip(image, mask, u=0.5):
    if np.random.random() < u:
        image = cv2.flip(image, 0)
        mask = cv2.flip(mask, 0)

    return image, mask




def skin_preprocess(img):
    #im = np.empty(img.shape)
    im = img.astype(float)/255
    #X = img[:,:,0]
    #Y = img[:,:,1]
    #Z = img[:,:,2]
    #im[:,:,0] = (X.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    #im[:,:,1] = (Y.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    #im[:,:,2] = (Z.astype(float) / (X.astype(float) + Y.astype(float) + Z.astype(float) + 10)) * 3
    return im

def mask_preprocess(img):
    im = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    im[im <  1] = 0
    im[im >=1 ] = 1
    #im = im.reshape((im.shape[0],im.shape[1],1))
    #im = np.zeros((img.shape[0],img.shape[1],1),np.uint8)
    #im[(img[:,:,0] + img[:,:,1] + img[:,:,2]) >THRESHOLD] = 1
    return im.reshape((im.shape[0], im.shape[1],1)).astype(float)


class threadsafe_generator(object):
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, gen):
        self.gen = gen
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.gen.__next__()


def threadsafe(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_generator(f(*a, **kw))
    return g

@threadsafe
def segmentation_generator(img_files, mask_files=None,
                           img_preproc_func=skin_preprocess, mask_preproc_func=None, batch_size=4,
                           shuff=False, shape=(768, 512), augmentation=False):  # write the definition of your data generator

    x_batch = np.zeros((batch_size, shape[0], shape[1], 3))
    y_batch = np.zeros((batch_size, shape[0], shape[1], 1))
    batch_count = 0
    while True:
        if shuff:
            img_files_s, mask_files_s = shuffle(img_files, mask_files)
        else:
            img_files_s, mask_files_s = img_files, mask_files

        for count, (img_file, mask_file) in enumerate(zip(img_files_s, mask_files_s)):
            img = cv2.imread(img_file)
            mask = cv2.imread(mask_file)

            if img.shape[0] != shape[0]:
                img_r = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
                mask_r = cv2.rotate(mask, cv2.ROTATE_90_CLOCKWISE)

                (h, w) = img_r.shape[:2]

                img = img_r[:, (w-shape[1])//2 : (w-shape[1])//2 + shape[1]]
                mask = mask_r[:, (w-shape[1])//2 : (w-shape[1])//2 + shape[1]]

            if augmentation:
                img = randomHueSaturationValue(img,
                                               hue_shift_limit=(-8, 8),
                                               sat_shift_limit=(-10, 10),
                                               val_shift_limit=(-30, 30))
                img, mask = randomShift(img, mask, shift_limit=(-0.1, 0.1))
                img, mask = randomHorizontalFlip(img, mask)
                img, mask = randomVerticalFlip(img, mask)

            img = img_preproc_func(img)
            mask = mask_preproc_func(mask)

            x_batch[batch_count] = img
            y_batch[batch_count] = mask
            batch_count += 1
            if batch_count == batch_size:
                yield x_batch, y_batch
                x_batch = np.zeros((batch_size, shape[0], shape[1], 3))
                y_batch = np.zeros((batch_size, shape[0], shape[1], 1))
                batch_count = 0


########## Data generator is now threadsafe and should work with multiple workers ##########

import keras.backend as K

def jaccard_index(y_true, y_pred):
    smooth = 1

    y_true_f = K.flatten(K.round(y_true))
    y_pred_f = K.flatten(K.round(y_pred))

    intersection = K.sum(y_true_f * y_pred_f)
    union = K.sum(y_true_f) + K.sum(y_pred_f) - intersection
    score = (intersection + smooth) / (union + smooth)
    return score




def dice_loss(y_true, y_pred):
    """Soft dice (Sørensen or Jaccard) coefficient for comparing the similarity
    of two batch of data, usually be used for binary image segmentation
    i.e. labels are binary. The coefficient between 0 to 1, 1 means totally match.

    Parameters
    -----------
    output : Tensor
        A distribution with shape: [batch_size, ....], (any dimensions).
    target : Tensor
        The target distribution, format the same with `output`.
    loss_type : str
        ``jaccard`` or ``sorensen``, default is ``jaccard``.
    axis : tuple of int
        All dimensions are reduced, default ``[1,2,3]``.
    smooth : float
        This small value will be added to the numerator and denominator.
            - If both output and target are empty, it makes sure dice is 1.
            - If either output or target are empty (all pixels are background), dice = ```smooth/(small_value + smooth)``, then if smooth is very small, dice close to 0 (even the image values lower than the threshold), so in this case, higher smooth can have a higher dice.

    Examples
    ---------
    # outputs = tl.act.pixel_wise_softmax(network.outputs)
    # dice_loss = 1 - tl.cost.dice_coe(outputs, y_)

    References
    -----------
    - `Wiki-Dice <https://en.wikipedia.org/wiki/Sørensen–Dice_coefficient>`__

    """
    smooth = 0.00001

    inse = K.sum(y_pred * y_true)
    l = K.sum(y_pred * y_pred)
    r = K.sum(y_true * y_true)

    dice = (2. * inse + smooth) / (l + r + smooth)

    return 1-dice




from keras.callbacks import TensorBoard

def get_callbacks(model_name, log_dir="/tmp/tensor_board", patience=30):
    es = EarlyStopping('val_jaccard_index', patience=patience, mode="max")
    msave = ModelCheckpoint(model_name+'.hdf5', save_best_only=True, save_weights_only=True)
    log = TensorBoard(log_dir=log_dir+"/"+model_name)
    #red = ReduceLROnPlateau(patience=patience//5, verbose=1, cooldown=patience//6)
    return [es, msave, log]#, red]

def get_run():
    if os.path.exists("runcount.csv"):
        rc = pd.read_csv("runcount.csv")
    else:
        rc=pd.DataFrame(data={'run':['0']}, dtype='int')
    run = rc.run[0]
    run += 1
    rc.run = [run]
    rc.to_csv("runcount.csv", index=False)
    return run
