import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os

from unet_512_768 import get_unet_512
from dnn_utils import segmentation_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################
BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

ori_path = 'images/sfa/ORI'
gt_path = 'images/sfa/GT'

img_files = os.listdir(ori_path)

train_subjects = list(range(1,783))
valid_subjects = list(range(783,951))
test_subjects = list(range(951,1119))

train_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_img =  [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in  test_subjects]

train_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_gt =  [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in  test_subjects]


TEST_BATCH_SIZE = 15

train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True,shuff=True)
valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)
test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=TEST_BATCH_SIZE)

'''
import cv2
img, mask = next(train_datagen)
im = (img[0] * 255).astype(np.uint8)
mk = (mask[0] * 255).astype(np.uint8)
cv2.imshow("Result", im)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()
'''


model = get_unet_512()

model.summary()

model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

model_name= 'unet'+ \
            '_LR' + str(LR) +\
            '_DC' + str(DECAY)

run = get_run()
model_name_run = model_name + "_" + str(run)

print("Model:", model_name_run)


model.fit_generator(
        train_datagen,
        steps_per_epoch=len(train_img)//BATCH_SIZE,
        epochs=100,
        validation_data=valid_datagen,
        validation_steps=len(valid_img)//BATCH_SIZE,
        callbacks=get_callbacks(model_name_run, log_dir="tensorboard",patience=30),
        workers=8 )

