from keras.optimizers import RMSprop
import os

from patch_net import get_patch_net2
from dnn_utils import patch_generator, evaluate_patch_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################
BATCH_SIZE = 1024
LR=0.0001
DECAY=0.002

ori_path = 'images/sfa/ORI'
gt_path = 'images/sfa/GT'

img_files = os.listdir(ori_path)

train_subjects = list(range(1,783))
valid_subjects = list(range(783,951))
test_subjects = list(range(951,1119))

train_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_img =  [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in  test_subjects]

train_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_gt =  [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in  test_subjects]


train_datagen =patch_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True)
valid_datagen = patch_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)
test_datagen = patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)



model = get_patch_net2()

model.summary()

model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

model_name= 'patch2_full'+ \
            '_LR' + str(LR) +\
            '_DC' + str(DECAY)

print("Model:", model_name)
run = get_run()
model_name_run = model_name + "_" + str(run)

model.fit_generator(
        train_datagen,
        steps_per_epoch=len(train_img)*32//BATCH_SIZE,
        epochs=100,
        validation_data=test_datagen,
        validation_steps=len(test_img)*32/BATCH_SIZE,
        callbacks=get_callbacks(model_name_run, log_dir="tensorboard",patience=30),
        workers=8 )

