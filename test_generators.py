import os
from dnn_utils import segmentation_generator, skin_preprocess, mask_preprocess, jaccard_index, get_run, get_callbacks
import cv2
BATCH_SIZE = 36





ori_path = 'images/SkinDataset/ORI'
gt_path = 'images/SkinDataset/GT'

img_files = os.listdir(ori_path)

test_subjects = ['243', '278']
valid_subjects = ['44', '24']

train_img = [os.path.join(ori_path, x) for x in img_files if not x[:-4] in test_subjects + valid_subjects]
test_img =  [os.path.join(ori_path, x) for x in img_files if x[:-4] in test_subjects]
valid_img = [os.path.join(ori_path, x) for x in img_files if x[:-4] in valid_subjects]

train_gt = [os.path.join(gt_path, x) for x in img_files if not x[:-4] in test_subjects + valid_subjects]
test_gt =  [os.path.join(gt_path, x) for x in img_files if x[:-4] in test_subjects]
valid_gt = [os.path.join(gt_path, x) for x in img_files if x[:-4] in valid_subjects]


train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True)
valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)
test_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)


imgs, masks = next(train_datagen)

for i,m in zip(imgs,masks):
    print(i.max(),i.min())
    print(m.max(),m.min())
    cv2.imshow("image",i)
    cv2.imshow("mask",m.reshape((m.shape[0],m.shape[1])))
    cv2.waitKey(0)

cv2.destroyAllWindows()

'''
mask = cv2.imread('images/SkinDataset/GT/429.jpg')

cv2.imwrite("mask429.jpg",mask_preprocess(mask))


cv2.imshow("mask", mask_preprocess(mask))
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break

'''
