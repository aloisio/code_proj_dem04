import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os
import cv2


from patch_net import get_patch_net, get_patch_net2
from dnn_utils import patch_generator, evaluate_patch_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks
from dnn_utils import eval_jaccard, eval_acc


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################

BATCH_SIZE = 1024
LR=0.0001
DECAY=0.002

ori_path = 'images/SkinDataset/ORI'
gt_path = 'images/SkinDataset/GT'

img_files = os.listdir(ori_path)

test_subjects = ['243', '278']
#valid_subjects = ['44', '24']

train_img = [os.path.join(ori_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_img =  [os.path.join(ori_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_img = [os.path.join(ori_path, x) for x in img_files if x[:-4] in valid_subjects]

train_gt = [os.path.join(gt_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_gt =  [os.path.join(gt_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_gt = [os.path.join(gt_path, x) for x in img_files if x[:-4] in valid_subjects]


train_datagen =patch_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True)
#valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
#                             mask_preproc_func=mask_preprocess, batch_size=1)
test_datagen = patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)

model = get_patch_net2()

model.summary()

model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

################################################
print("Predicting...")
################################################

model_name = "patch2_LR0.0001_DC0.002_62"

model.load_weights(model_name + ".hdf5")


eval_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess)


TEST_STEPS = len(test_img)
acc, jac = 0,0

tn_h, tn_w = 768//2, 512//2
grid_size = (2,3)

im_result = np.zeros((tn_h*grid_size[0],tn_w*grid_size[1],3),np.uint8)

for i in range(TEST_STEPS):

    print("retrieving patchs %d..."%(i+1))
    patch, mask = next(eval_datagen)

    print("predicting %d..."%(i+1))
    p = model.predict(patch)

    img = cv2.imread(test_img[i])

    acc += eval_acc(mask, p)
    jac += eval_jaccard(mask, p)
    print("Image %d Acc: %.4f  Jaccard Index: %.4f" % (i, eval_acc(mask, p), eval_jaccard(mask, p)))

    lin = (i*3)//grid_size[0]
    col = (i*3)%grid_size[1]

    y_start = lin * tn_h
    x_start = col * tn_w*3


    img = cv2.resize(img,(tn_w,tn_h))
    im_result[y_start:y_start+tn_h, x_start:x_start+tn_w] = img

    pred = cv2.resize((p.reshape(mask.shape) * 255).astype(np.uint8),(tn_w,tn_h))
    mask = cv2.resize((mask*255).astype(np.uint8),(tn_w,tn_h))

    for channel in range(3):
        im_result[y_start:y_start+tn_h, x_start+tn_w:x_start+2*tn_w, channel] = mask
        im_result[y_start:y_start+tn_h, x_start+2*tn_w:x_start+3*tn_w, channel] = pred


acc, jac = acc/TEST_STEPS, jac/TEST_STEPS

print("Final Result. Acc: %.4f  Jaccard Index: %.4f" % (acc, jac))

cv2.imshow("Result", im_result)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()
cv2.imwrite(model_name+".png",im_result)

