import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os

from unet_512_768 import get_unet_512
from dnn_utils import segmentation_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################
BATCH_SIZE = 3
LR=0.0001
DECAY=0.002

ori_path = 'images/sfa/ORI'
gt_path = 'images/sfa/GT'

img_files = os.listdir(ori_path)

#train_subjects = list(range(1,677))
#valid_subjects = list(range(677,777))
#test_subjects = list(range(777,877))

train_subjects = list(range(1,783))
valid_subjects = list(range(783,951))
test_subjects = list(range(951,1119))

train_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_img =  [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in  test_subjects]

train_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_gt =  [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in  test_subjects]


TEST_BATCH_SIZE = 15

train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True, shuff=True)
valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)
test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=TEST_BATCH_SIZE, augmentation=False)


import cv2


img_result = np.zeros((960,1152,3),np.uint8)

for j in range(TEST_BATCH_SIZE):
        img, mask = next(train_datagen)

        row = j // 3
        col = j % 3

        im = cv2.resize((img[0]*255).astype(np.uint8), (128,192))
        x_start = (col) * 128 * 3
        x_end = (col) * 128 * 3 + 128
        y_start = row * 192
        y_end = (row + 1) * 192
        img_result[y_start:y_end, x_start:x_end]  = im

        im = cv2.resize((img[1]*255).astype(np.uint8), (128,192))
        x_start = (col) * 128 * 3 + 128
        x_end = (col) * 128 * 3 + 128*2
        y_start = row * 192
        y_end = (row + 1) * 192
        img_result[y_start:y_end, x_start:x_end]  = im

        im = cv2.resize((img[2]*255).astype(np.uint8), (128,192))
        x_start = (col) * 128 * 3 + 128*2
        x_end = (col) * 128 * 3 + 128*3
        y_start = row * 192
        y_end = (row + 1) * 192
        img_result[y_start:y_end, x_start:x_end]  = im


cv2.imshow("Augmentation", img_result)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()

#cv2.imwrite("augmentation.jpg", img_result)


