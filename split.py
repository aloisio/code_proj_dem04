# splits a dataset into train, val, test
# val and test has same size
import numpy as np
from sklearn.model_selection import train_test_split

DATASET_SIZE = 1118
TEST_PROPORTION = 0.15

test_size = int(DATASET_SIZE * TEST_PROPORTION)

x = np.arange(DATASET_SIZE) + 1

x, x_test = train_test_split(x, test_size=test_size, random_state=111)
x_train, x_val = train_test_split(x, test_size=test_size, random_state=222)

#save csv files
np.savetxt("train.csv", x_train, delimiter=",",fmt='%d')
np.savetxt("val.csv", x_val, delimiter=",",fmt='%d')
np.savetxt("test.csv", x_test, delimiter=",",fmt='%d')

