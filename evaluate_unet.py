import numpy as np # linear algebra
from keras.optimizers import RMSprop
import os

from unet_512_768 import get_unet_512
from dnn_utils import evaluation_generator, segmentation_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks
from dnn_utils import eval_jaccard, eval_acc
import cv2
import time

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################
BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

ori_path = 'images/SkinDataset/ORI'
gt_path = 'images/SkinDataset/GT'

img_files = os.listdir(ori_path)

test_subjects = ['243', '278']
#valid_subjects = ['44', '24']

train_img = [os.path.join(ori_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_img =  [os.path.join(ori_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_img = [os.path.join(ori_path, x) for x in img_files if x[:-4] in valid_subjects]

train_gt = [os.path.join(gt_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_gt =  [os.path.join(gt_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_gt = [os.path.join(gt_path, x) for x in img_files if x[:-4] in valid_subjects]


train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True)
#valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
#                             mask_preproc_func=mask_preprocess, batch_size=1)
test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)


model = get_unet_512()

model.summary()

model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

model_name= "unet_LR0.0001_DC0.002_30"


################################################
print("\nPredicting...\n")
################################################

#model.load_weights("../furniture/"+model_name_run+".hdf5")
model.load_weights(model_name+".hdf5")


################################################
print("Evaluating",model_name,"...")
################################################

start_time = time.time()

eval_datagen = evaluation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)

TEST_STEPS = len(test_img)

acc = 0
jac = 0
for i in range(TEST_STEPS):
    img, mask = next(eval_datagen)

    pred = model.predict(img)

    img=img[0]
    mask=mask[0]
    pred=pred[0]

    if pred.shape[0] != mask.shape[0]:

        pred_corr = np.zeros(mask.shape)

        (h1, w1) = pred.shape[:2]
        (h2, w2) = mask.shape[:2]

        pred_corr[:, (w2 - w1) // 2: (w2 - w1) // 2 + w1] = pred[(h1 - h2) // 2: (h1 - h2) // 2 + h2, :]
        pred = pred_corr

    acc += eval_acc(mask, pred)
    jac += eval_jaccard(mask, pred)
    print("Acc: %.4f  Jaccard Index: %.4f" % (eval_acc(mask, pred), eval_jaccard(mask, pred)))

    im = (img*255).astype(np.uint8)
    mk = (mask*255).astype(np.uint8)
    p = (pred*255).astype(np.uint8)

print("\nFinal Results %s.  Acc: %.4f  Jaccard Index: %.4f" % (model_name, acc/TEST_STEPS,  jac/TEST_STEPS))

print("\nExecution time: %.2f seconds" % (time.time() - start_time))





TEST_STEPS = len(test_img)
acc, jac = 0,0

tn_h, tn_w = 768//2, 512//2
grid_size = (2,3)

im_result = np.zeros((tn_h*grid_size[0],tn_w*grid_size[1],3),np.uint8)

start_time = time.time()

for i in range(TEST_STEPS):

    img, mask = next(test_datagen)

    p = model.predict(img)

    img = cv2.imread(test_img[i])

    acc += eval_acc(mask, p)
    jac += eval_jaccard(mask, p)
    print("Image %d Acc: %.4f  Jaccard Index: %.4f" % (i, eval_acc(mask, p), eval_jaccard(mask, p)))

    lin = (i*3)//grid_size[0]
    col = (i*3)%grid_size[1]

    y_start = lin * tn_h
    x_start = col * tn_w*3

    mask = (mask[0]*255).astype(np.uint8)
    p = (p[0]*255).astype(np.uint8)


    img = cv2.resize(img,(tn_w,tn_h))
    im_result[y_start:y_start+tn_h, x_start:x_start+tn_w] = img

    p = cv2.resize(p,(tn_w,tn_h))
    mask = cv2.resize(mask,(tn_w,tn_h))

    for channel in range(3):
        im_result[y_start:y_start+tn_h, x_start+tn_w:x_start+2*tn_w, channel] = mask
        im_result[y_start:y_start+tn_h, x_start+2*tn_w:x_start+3*tn_w, channel] = p

    #print(mask.shape, pred.shape)

acc, jac = acc/TEST_STEPS, jac/TEST_STEPS

print("\nFinal Result. Acc: %.4f  Jaccard Index: %.4f" % (acc, jac))
print("\nExecution time: %.2f seconds" % (time.time() - start_time))

cv2.imshow("Result", im_result)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()
cv2.imwrite(model_name+".png",im_result)

