import numpy as np # linear algebra
from keras.optimizers import RMSprop
import os
import cv2
import time

from unet_512_768 import get_unet_512
from dnn_utils import segmentation_generator, evaluation_generator, skin_preprocess, mask_preprocess
from dnn_utils import dice_loss, jaccard_index


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################
BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

ori_path = 'images/sfa/ORI'
gt_path = 'images/sfa/GT'

img_files = os.listdir(ori_path)

train_subjects = list(range(1,783))
valid_subjects = list(range(783,951))
test_subjects = list(range(951,1119))

train_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_img = [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_img =  [os.path.join(ori_path, "img ("+str(x)+").jpg") for x in  test_subjects]

train_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in train_subjects]
valid_gt = [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in valid_subjects]
test_gt =  [os.path.join(gt_path, "img ("+str(x)+").jpg") for x in  test_subjects]




model = get_unet_512()

model.summary()
model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])


def eval_jaccard(y_true, y_pred):
    smooth = 1

    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()

    y_pred_f[y_pred_f>.5]=1
    y_pred_f[y_pred_f<=.5]=0

    intersection = np.sum(y_true_f * y_pred_f)
    union = np.sum(y_true_f) + np.sum(y_pred_f) - intersection
    score = (intersection + smooth) / (union + smooth)
    return score

def eval_acc(y_true, y_pred):
    smooth = 1

    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()

    y_pred_f[y_pred_f>.5]=1
    y_pred_f[y_pred_f<=.5]=0

    score = len(y_true_f[y_true_f==y_pred_f])/len(y_true_f)
    return score

model_name="unet_LR0.0001_DC0.002_43"
model.load_weights(model_name+".hdf5")

################################################
print("Evaluating",model_name,"...")
################################################

start_time = time.time()

eval_datagen = evaluation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)

TEST_STEPS = len(test_img)

acc = 0
jac = 0
for i in range(TEST_STEPS):
    img, mask = next(eval_datagen)

    pred = model.predict(img)

    img=img[0]
    mask=mask[0]
    pred=pred[0]

    if pred.shape[0] != mask.shape[0]:

        pred_corr = np.zeros(mask.shape)

        (h1, w1) = pred.shape[:2]
        (h2, w2) = mask.shape[:2]

        pred_corr[:, (w2 - w1) // 2: (w2 - w1) // 2 + w1] = pred[(h1 - h2) // 2: (h1 - h2) // 2 + h2, :]
        pred = pred_corr

    acc += eval_acc(mask, pred)
    jac += eval_jaccard(mask, pred)
    print("Acc: %.4f  Jaccard Index: %.4f" % (eval_acc(mask, pred), eval_jaccard(mask, pred)))

    im = (img*255).astype(np.uint8)
    mk = (mask*255).astype(np.uint8)
    p = (pred*255).astype(np.uint8)

print("\nFinal Results %s.  Acc: %.4f  Jaccard Index: %.4f" % (model_name, acc/TEST_STEPS,  jac/TEST_STEPS))

print("\nExecution time: %.2f seconds" % (time.time() - start_time))


################################################
print("Generating Thumbnails",model_name,"...")
################################################

n_thumbs = 15

test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=n_thumbs)

img, mask = next(test_datagen)

pred = model.predict(img)

img_result = np.zeros((960,1152,3),np.uint8)

for j in range(n_thumbs):

        im = cv2.resize((img[j]*255).astype(np.uint8), (128,192))
        mk = cv2.resize((mask[j]*255).astype(np.uint8), (128,192))
        p = cv2.resize((pred[j]*255).astype(np.uint8), (128,192))

        row = j // 3
        col = j % 3

        x_start = (col) * 128 * 3
        x_end = (col) * 128 * 3 + 128
        y_start = row * 192
        y_end = (row + 1) * 192
        img_result[y_start:y_end, x_start:x_end]  = im

        x_start = (col) * 128 * 3 + 128
        x_end = (col) * 128 * 3 + 128*2
        y_start = row * 192
        y_end = (row + 1) * 192
        img_result[y_start:y_end, x_start:x_end, 0]  = mk
        img_result[y_start:y_end, x_start:x_end, 1]  = mk
        img_result[y_start:y_end, x_start:x_end, 2]  = mk

        x_start = (col) * 128 * 3 + 128*2
        x_end = (col) * 128 * 3 + 128*3
        y_start = row * 192
        y_end = (row + 1) * 192
        img_result[y_start:y_end, x_start:x_end, 0]  = p
        img_result[y_start:y_end, x_start:x_end, 1]  = p
        img_result[y_start:y_end, x_start:x_end, 2]  = p

cv2.imwrite(model_name+".png", img_result)

cv2.imshow("Result", img_result)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()




