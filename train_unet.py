import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os

from unet_512_768 import get_unet_512
from dnn_utils import segmentation_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################
BATCH_SIZE = 4
LR=0.0001
DECAY=0.002

train_path = 'CalTech101/train'
test_path = 'CalTech101/train'

train_files = os.listdir(train_path)

test_subjects = ['243', '278']
#valid_subjects = ['44', '24']

train_img = [os.path.join(ori_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_img =  [os.path.join(ori_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_img = [os.path.join(ori_path, x) for x in img_files if x[:-4] in valid_subjects]

train_gt = [os.path.join(gt_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_gt =  [os.path.join(gt_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_gt = [os.path.join(gt_path, x) for x in img_files if x[:-4] in valid_subjects]


train_datagen = segmentation_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True)
#valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
#                             mask_preproc_func=mask_preprocess, batch_size=1)
test_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)


model = get_unet_512()

model.summary()

model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

model_name= 'unet'+ \
            '_LR' + str(LR) +\
            '_DC' + str(DECAY)

print("Model:", model_name)
run = get_run()
model_name_run = model_name + "_" + str(run)

model.fit_generator(
        train_datagen,
        steps_per_epoch=10,
        epochs=1000,
        validation_data=test_datagen,
        validation_steps=len(test_img),
        callbacks=get_callbacks(model_name_run, log_dir="tensorboard",patience=75),
        workers=8 )


################################################
print("Predicting...")
################################################

#model.load_weights("../furniture/"+model_name_run+".hdf5")
model.load_weights("unet_LR0.0001_DC0.002_30.hdf5")

TEST_BATCH_SIZE = 2



import cv2

for i in range(TEST_BATCH_SIZE):

    img, mask = next(test_datagen)

    p = model.predict(img)

    print("jaccard", jaccard_index(mask.astype(np.float32),p.astype(np.float32)))


    img = (img[0]*255).astype(np.uint8)
    mask = (mask[0]*255).astype(np.uint8)
    p = (p[0]*255).astype(np.uint8)


    cv2.imshow("image", img)
    cv2.imshow("mask", mask)
    cv2.imshow("pred", p)
    while True:
        c = cv2.waitKey(3) & 0xFF
        if c in [ord('n'), ord('s')]:
            break
cv2.destroyAllWindows()
