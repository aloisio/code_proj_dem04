import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os

from patch_net import get_patch_net, get_patch_net2
from dnn_utils import patch_generator, evaluate_patch_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index, get_run, get_callbacks


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################


BATCH_SIZE = 1024
LR=0.0001
DECAY=0.002

ori_path = 'images/SkinDataset/ORI'
gt_path = 'images/SkinDataset/GT'

img_files = os.listdir(ori_path)

test_subjects = ['243', '278']
#valid_subjects = ['44', '24']

train_img = [os.path.join(ori_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_img =  [os.path.join(ori_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_img = [os.path.join(ori_path, x) for x in img_files if x[:-4] in valid_subjects]

train_gt = [os.path.join(gt_path, x) for x in img_files if not x[:-4] in test_subjects] #+ valid_subjects]
test_gt =  [os.path.join(gt_path, x) for x in img_files if x[:-4] in test_subjects]
#valid_gt = [os.path.join(gt_path, x) for x in img_files if x[:-4] in valid_subjects]


train_datagen =patch_generator(train_img, mask_files=train_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE, augmentation=True)
#valid_datagen = segmentation_generator(valid_img, mask_files=valid_gt, img_preproc_func=skin_preprocess,
#                             mask_preproc_func=mask_preprocess, batch_size=1)
test_datagen = patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)


model = get_patch_net2()

model.summary()

model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

model_name= 'patch2'+ \
            '_LR' + str(LR) +\
            '_DC' + str(DECAY)

print("Model:", model_name)
run = get_run()
model_name_run = model_name + "_" + str(run)

model.fit_generator(
        train_datagen,
        steps_per_epoch=10,
        epochs=100,
        validation_data=test_datagen,
        validation_steps=1,
        callbacks=get_callbacks(model_name_run, log_dir="tensorboard",patience=30),
        workers=8 )


################################################
print("Predicting...")
################################################

model.load_weights(model_name_run+".hdf5")
#model.load_weights("patch_LR0.0001_DC0.002_53.hdf5")


eval_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess)


TEST_STEPS = len(test_img)

import cv2

for i in range(TEST_STEPS):

    img, mask = next(eval_datagen)


    print("wait prediction...")
    p = model.predict(img)

    print(p.shape)

    #print("jaccard", jaccard_index(mask.astype(np.float32),p.astype(np.float32)))


    mask = (mask*255).astype(np.uint8)
    pred = (p.reshape(mask.shape) *255).astype(np.uint8)

    print(mask.shape, pred.shape)


    cv2.imshow("mask", mask)
    cv2.imshow("pred", pred)
    while True:
        c = cv2.waitKey(3) & 0xFF
        if c in [ord('n'), ord('s')]:
            break
cv2.destroyAllWindows()
