import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.optimizers import RMSprop
import os
import cv2
import time

from patch_net import get_patch_net2
from unet_512_768 import get_unet_512
from dnn_utils import patch_generator, evaluate_patch_generator, skin_preprocess, mask_preprocess, dice_loss, jaccard_index
from dnn_utils import eval_jaccard, eval_acc, segmentation_generator


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

##################################

BATCH_SIZE = 1024
LR=0.0001
DECAY=0.002

ori_path = 'images/SkinDataset/ORI'
gt_path = 'images/SkinDataset/GT'

img_files = os.listdir(ori_path)

test_subjects = ['243', '278']

train_img = [os.path.join(ori_path, x) for x in img_files if not x[:-4] in test_subjects]
test_img =  [os.path.join(ori_path, x) for x in img_files if x[:-4] in test_subjects]

train_gt = [os.path.join(gt_path, x) for x in img_files if not x[:-4] in test_subjects]
test_gt =  [os.path.join(gt_path, x) for x in img_files if x[:-4] in test_subjects]


patch_datagen = patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=BATCH_SIZE)

patch_model = get_patch_net2()
patch_model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

unet_model = get_unet_512()
unet_model.compile(optimizer=RMSprop(lr=LR, decay=DECAY), loss=dice_loss, metrics=[jaccard_index,'accuracy' ])

################################################
print("Predicting...")
################################################

patch_model_name = "patch2_LR0.0001_DC0.002_70"

patch_model.load_weights(patch_model_name + ".hdf5")

unet_model_name = "unet_LR0.0001_DC0.002_30"

unet_model.load_weights(unet_model_name + ".hdf5")


patch_datagen = evaluate_patch_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess)

unet_datagen = segmentation_generator(test_img, mask_files=test_gt, img_preproc_func=skin_preprocess,
                             mask_preproc_func=mask_preprocess, batch_size=1)


TEST_STEPS = len(test_img)
acc_patch, jac_patch = 0,0
acc_unet, jac_unet = 0,0

tn_h, tn_w = 768//2, 512//2
grid_size = (2,4)

im_result = np.zeros((tn_h*grid_size[0],tn_w*grid_size[1],3),np.uint8)

for i in range(TEST_STEPS):

    print("\nPredicting image %s" % test_img[i])

    img = cv2.imread(test_img[i])
    img = cv2.resize(img,(tn_w,tn_h))

    # Patch Prediction

    start_time = time.time()
    patch, mask = next(patch_datagen)
    print(" .")
    patch_prediction = patch_model.predict(patch)
    a = eval_acc(mask, patch_prediction)
    j = eval_jaccard(mask, patch_prediction)
    print("Patch Prediction Acc: %.4f  Jaccard Index: %.4f Time: %.2f seconds" % (a, j, time.time() - start_time))
    acc_patch += a
    jac_patch += j

    # UNet Prediction

    start_time = time.time()
    _img, _mask = next(unet_datagen)
    unet_prediction = unet_model.predict(_img)
    a = eval_acc(mask, unet_prediction)
    j = eval_jaccard(mask, unet_prediction)
    print("U-Net Prediction Acc: %.4f  Jaccard Index: %.4f Time: %.2f seconds" % (eval_acc(mask, unet_prediction), eval_jaccard(mask, unet_prediction),time.time() - start_time))
    acc_unet += a
    jac_unet += j

    # Thumbnails

    lin = (i*4)//grid_size[1]
    col = (i*4)%grid_size[1]

    y_start = lin * tn_h
    x_start = col * tn_w

    im_result[y_start:y_start+tn_h, x_start:x_start+tn_w] = img

    patch_pred = cv2.resize((patch_prediction .reshape(mask.shape) * 255).astype(np.uint8),(tn_w,tn_h))
    unet_pred = cv2.resize((unet_prediction[0] * 255).astype(np.uint8),(tn_w,tn_h))

    mask = cv2.resize((mask*255).astype(np.uint8),(tn_w,tn_h))

    for channel in range(3):
        im_result[y_start:y_start+tn_h, x_start+tn_w:x_start+2*tn_w, channel] = mask
        im_result[y_start:y_start+tn_h, x_start+2*tn_w:x_start+3*tn_w, channel] = patch_pred
        im_result[y_start:y_start+tn_h, x_start+3*tn_w:x_start+4*tn_w, channel] = unet_pred


acc_patch, jac_patch = acc_patch/TEST_STEPS, jac_patch/TEST_STEPS
acc_unet, jac_unet = acc_unet/TEST_STEPS, jac_unet/TEST_STEPS

print("\nFinal Results." )
print("Patch - Acc: %.4f  Jaccard Index: %.4f" % (acc_patch, jac_patch))
print("Unet  - Acc: %.4f  Jaccard Index: %.4f" % (acc_unet, jac_unet))

cv2.imshow("Result", im_result)
while True:
    c = cv2.waitKey(3) & 0xFF
    if c in [ord('n'), ord('s')]:
        break
cv2.destroyAllWindows()
cv2.imwrite("small_sample.png",im_result)

